package Task3;

public class FigureSquarePerimeter extends Figure {
    private int height;

    public FigureSquarePerimeter(int h) {
    height =h;
    h =7;
}



    @Override
    public int calcSquare() {
        return height*height;
    }

    @Override
    public int calcPerimeter() {
        return height*4;
    }

    public void printCalc(){
        System.out.println("Area is:" + calcSquare());
        System.out.println("Perimeter is:" + calcPerimeter());
    }
}
